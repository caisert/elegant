<?php defined( 'ABSPATH' ) or die ( 'Error de solicitud' );?>
<?php get_header();?>
    <!-- contenido -->  
    <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>

            <!-- Titulo de la página-->
            <h2><?php the_title(); ?></h2>

            <!-- Contenido de la página -->
            <div class="">
                <p><?php the_content(); ?></p>
            </div>

        <?php endwhile; ?>
    <?php endif; ?>
<?php get_footer();?>
