<?php /* Template Name: Contact Us */ ?>
<?php defined( 'ABSPATH' ) or die ( 'No script kiddles please!' );?>

<?php

//response generation function
$response = "";

//function to generate response
function my_contact_form_generate_response($type, $message){

    global $response;

    if($type == "success") $response = "<div class='success'>{$message}</div>";
    else $response = "<div class='error'>{$message}</div>";
}

// Mensajes de respuesta
$not_human       = "Falló la verificación humana.";
$missing_content = "Por favor rellena toda la información.";
$email_invalid   = "E-mail incorrecto.";
$message_unsent  = "Mensaje no enviado. Intenta de nuevo.";
$message_sent    = "Gracias! Tu mensaje ha sido enviado.";

// Variables enviadas por post
$name = $_POST['message_name'];
$email = $_POST['message_email'];
$message = $_POST['message_text'];
$human = $_POST['message_human'];

// Variables php mailer
$to = get_option('admin_email');
$subject = "Mensaje from ".get_bloginfo('name');
$headers = 'From: '. $email . "\r\n" .
    'Reply-To: ' . $email . "\r\n";

if(!$human == 0){
    if($human != 2)
        my_contact_form_generate_response("error", $not_human);
    else {
        //validate email
        if(!filter_var($email, FILTER_VALIDATE_EMAIL))
            my_contact_form_generate_response("error", $email_invalid);
        else {
            //validate presence of name and message
            if(empty($name) || empty($message)){
                my_contact_form_generate_response("error", $missing_content);
            }
            else {//ready to go!
                $sent = wp_mail($to, $subject, strip_tags($message), $headers);
                if($sent)
                    my_contact_form_generate_response("success", $message_sent);
                else my_contact_form_generate_response("error", $message_unsent);
            }
        }
    }
}
else if ($_POST['submitted']) my_contact_form_generate_response("error", $missing_content);

?>

<?php get_header(); ?>

<div id="primary" class="site-content">
    <div id="content" role="main">

    <?php while ( have_posts() ) : the_post(); ?>

        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="w3-section w3-container">
                <header class="entry-header w3-center">
                    <h2 class="entry-title goudsanm text-azulel w3-xxxlarge">
                        <?php 
                            $the_title = mb_strtoupper(get_the_title(), 'UTF-8'); 
                            echo $the_title; 
                        ?>
                    </h2>
                </header>
    
                <div id="contact-content" class="entry-content padding-xl_lr text-justify interlineado-simple goudos text-dark-grey font-size_16">
                    <?php the_content(); ?>
                </div><!-- .entry-content -->
            </div>
            
            <div id="contact-form" class="w3-section w3-container w3-center">           
                <?php if ($response): ?>
                    <div class="w3-row parent-flex">
                        <div class="w3-col s10 m8 l7 child-flex w3-container">
                            <div id="mensaje-error" class="w3-panel w3-display-container cursor-pointer">
                                <span class="goudos" onclick="this.parentElement.style.display='none'"><h3><?php echo $response; ?></h3></span>
                            </div><!-- Mensaje de validación -->
                        </div>
                    </div>
                <?php endif; ?>
                <div class="w3-row parent-flex">
                    <div class="w3-col s12 m10 l6 child-flex">
                        <div class="w3-row">
                            <form action="<?php the_permalink(); ?>" method="post" class="w3-container padding-md">
                                <div class="w3-mobile w3-col m6 margin-xs_tb">
                                    <label for="name" class="goudos text-dark-grey font-size_16">Nombre: <span>*</span>
                                    <input type="text" name="message_name" value="<?php echo esc_attr($_POST['message_name']); ?>" class="input-color w3-round" required></label>
                                </div>
        
                                <div class="w3-mobile w3-col m6 margin-xs_tb">
                                    <label for="message_email" class="goudos text-dark-grey font-size_16">Email: <span>*</span>
                                    <input type="text" name="message_email" value="<?php echo esc_attr($_POST['message_email']); ?>" class="input-color w3-round" required></label>
                                </div>

                                <div class="w3-mobile w3-col m12 margin-xs_tb">
                                    <label for="message_text" class="goudos text-dark-grey font-size_16">Mensaje: <span>*</span><br>
                                    <textarea type="text" rows="2" name="message_text" class="input-color w3-round" required><?php echo esc_textarea($_POST['message_text']); ?></textarea></label>
                                </div>
                                
                                <div class="w3-mobile w3-col m12 margin-xs_tb">
                                    <label for="message_human" class="goudos text-dark-grey font-size_16">Verificación humana: <span>*</span>                        
                                    <input type="text" name="message_human" class="w3-center input-color w3-round no-padding human-verify" required> + 3 = 5</label>
                                </div>
                                   

                                <input type="hidden" name="submitted" value="1">
        
                                <div class="w3-mobile w3-col margin-xl_tb">
                                    <input type="submit" class="submit-button">                                    
                                </div>
        
                            </form>
                        </div>
                    </div>
                </div>
                    
            </div> <!-- #contact-form -->

        </article><!-- #post -->

    <?php endwhile; // end of the loop. ?>

    </div><!-- #content -->
</div><!-- #primary -->

<?php // get_sidebar(); ?>

<?php get_footer(); ?>
