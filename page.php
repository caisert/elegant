<?php defined( 'ABSPATH' ) or die ( 'Error de solicitud' );?>

<?php get_header();?>

    <!-- contenido -->  

    <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>

            <!-- Titulo de la página-->
            <h2><?php the_title(); ?></h2>

            <!-- Contenido de la página -->
            <div class="">
                <p><?php the_content(); ?></p>
            </div>
            <?php
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
                endif;
            ?>
        <?php endwhile; ?>
    <?php endif; ?>

<?php get_footer();?>
