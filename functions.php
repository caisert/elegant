<?php
    // Crear menu superior
    if (function_exists('register_nav_menus')) {
        register_nav_menus(array('superior' => 'Menu principal superior'));
    }

    // Crear clase para <a>
    add_filter('nav_menu_link_attributes', 'clases_menu_superior', 10, 3);

    // Se agregan las clases que lleva cada elemento del menu (los <li> del <ul>)
    function clases_menu_superior ($atts, $item, $args) {
        $class = 'w3-right w3-bar-item';
        $atts['class'] = $class;
        return $atts;
    }
    
    // Funcionalidad de logo personalizado
    function custom_logo_setup() {
        $defaults = array(
            'height'      => 100,
            'width'       => 400,
            'flex-height' => true,
            'flex-width'  => true,
            'header-text' => array( 'site-title', 'site-description' ),
        );
        add_theme_support( 'custom-logo', $defaults );
    }
    add_action( 'after_setup_theme', 'custom_logo_setup' );
    

    // Menú activo
    add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

    function special_nav_class ($classes, $item) {
        if (!in_array('current-menu-item', $classes) ){
            $classes[] = '';
        }
        return $classes;
    }

    // Agregar imagenes destacadas
    if ( function_exists( 'add_theme_support' ) ) {
        add_theme_support( 'post-thumbnails' );
        // Tamaño de las imágenes en caso de no establecerlo con css
        // set_post_thumbnail_size( 150, 150, true ); // default Featured Image dimensions (cropped)
     
        // // additional image sizes
        // // delete the next line if you do not need additional image sizes
        // add_image_size( 'category-thumb', 300, 9999 ); // 300 pixels wide (and unlimited height)
    }

    // Crear el tipo de post catalogo
    /* function create_posttype() {
        register_post_type( 'catalogo',
          array(
            'labels' => array(
              'name' => __( 'Catálogo' ),
              'singular_name' => __( 'Catálogo' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'catalogo'),
            'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),        
          )
        );
    }
    add_action( 'init', 'create_posttype' ); */

    // Agregar Paneles Laterales o Sidebar
    /* add_action( 'widgets_init', 'register_sidebars' );
    function my_register_sidebars() {
        // Register the 'primary' sidebar.
        my_register_sidebar(
            array(
                'id'            => 'catalogo',
                'name'          => __( 'Sidebar de Catálogo' ),
                'description'   => __( 'Panel lateral derecho para mostrar otros elementos del catálogo.' ),
                'before_widget' => '<div id="%1$s" class="widget %2$s my-3">',
                'after_widget'  => '</div>',
                'before_title'  => '<h3 class="widget-title">',
                'after_title'   => '</h3>',
            )
        );
        // Repeat register_sidebar() code for additional sidebars.
    } */

      
    //Función para personalizar comentarios
    function personalizar_comentarios ($comment, $args, $depth) {
        $GLOBALS['comment'] = $comment; ?>
        <li class="w3-bar" <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
            <div id="comment-<?php comment_ID(); ?>" class="comment-nivel-1">
                <div class="">
                    <?php echo get_avatar($comment,$size='48',$default=''); ?>
                    <?php printf( __( '<cite class="">%s</cite> <span class="says">dice:</span>' ), get_comment_author_link() ); ?>
                </div>

                <?php if ($comment->comment_aproved == '0') : ?>
                    <em><?php _e('Tu comentario está a la espera de la aprobación') ?></em>
                    <br/>
                <?php endif; ?>

                <div class=""><a href="<?php echo htmlspecialchars( get_comment_link($comment->comment_ID))?>"><?php printf(__('%1$s at %2$s'), get_comment_date(), get_comment_time()) ?><a/></div>
                
                <?php comment_text() ?>

                <div class="w3-left w3-ripple">
                    <?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                </div>
                <div class="w3-right w3-ripple"><?php edit_comment_link(__('Editar Comentario'), ' ', '') ?></div>
                
                <br>
            </div>
            <?php 
      }

    // Reiniciar las reglas de rewrite
    //   flush_rewrite_rules( false );
?>