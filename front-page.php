<?php defined( 'ABSPATH' ) or die ( 'Error de solicitud' );?>
<?php get_header();?>
    <!-- contenido del admin -->  
    <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
            <!-- Titulo de la página-->
            <h2><a class="" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

            <!-- Contenido de la página -->
            <div class="">
                <p><?php the_content(); ?></p>
            </div>

        <?php endwhile; ?>
    <?php endif; ?>
    <!-- contenido estático -->
    <h2>Esto es el front page</h2>
<?php get_footer();?>