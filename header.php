<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">

        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>
        <header>
            <!-- Logo -->
            <?php
                $custom_logo_id = get_theme_mod( 'custom_logo' );
                $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                if ( has_custom_logo() ) {
                    echo '<img src="'. esc_url( $logo[0] ) .'" class="w3-left w3-white" width="130px" height="60px" alt="Logo de '. get_bloginfo( 'name' ) .'">';
                    /* La variable $text_logo sirve como bandera para saber si hay una imagen de logo */
                    $text_logo = false;
                } else {
                    $text_logo = true;
                }
            ?>

            <!-- Menú -->						
            <?php if ( $text_logo == 1 ) { 
                echo '<span class="w3-left margin-xl_lr margin-md-t text-azulel">'. get_bloginfo( 'name' ) .'</span>';
            } 
            ?>

            <?php wp_nav_menu(array(
                'theme_location' => 'superior',
                'container' => 'nav',
                'container_id' => 'navbarSupportedContent',
                'items_wrap' => '<ul class="menu menu-superior no-margin margin-sm-t">%3$s</ul>',
                'menu_class' => 'w3-bar-item w3-button w3-bar-item w3-button w3-right'
            )); ?>
            <span>Esto es header</span>
        </header>