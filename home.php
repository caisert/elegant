<?php defined( 'ABSPATH' ) or die ( 'Error de solicitud' );?>

<?php get_header();?>
    <!-- contenido -->  
    <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>

            <!-- Foto -->
            <?php if ( get_the_post_thumbnail() ) { 
                the_post_thumbnail( 'post-thumbnail', array( 'class' => 'blogimg', 'alt' => 'Imagen alusiva a la entrada ' . get_the_title(), 'title' => get_the_title() ) );
            }
            else { ?> 
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" class="w3-image padding-xl-tb" alt="Logo" title="Logo"> <?php
            } ?>
            <!-- Titulo de la página-->
            <h2><a class="" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

            <!-- Contenido de la página -->
            <div class="">
                <p><?php the_excerpt(); ?></p>
            </div>

        <?php endwhile; ?>
    <?php endif; ?>
<?php get_footer();?>
