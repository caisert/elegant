<!--Para evitar acceder directamente a Comments.php-->
<?php if(!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME'])) : ?>  
    <?php die('No puedes acceder directamente a este archivo.'); ?>  
<?php endif; ?>

<!--Verificar si se necesita contraseña para ver los comentarios-->
<?php if(!empty($post->post_password)) : ?>  
    <?php if($_COOKIE['wp-postpass_' . COOKIEHASH] != $post->post_password) : ?>  
    <?php endif; ?>
<?php endif; ?>

<!--Mostrar Comentarios-->
<?php if ( have_comments() ) : ?>
    <section id="comentarios" class="">
        <h4 id="comments"><?php comments_number('No hay Comentarios', '1 Comentario', 'Comentarios <span class="">%</span>'); ?></h4>
        <ul class="">
            <?php wp_list_comments('callback=personalizar_comentarios'); ?>
        </ul>
    </section>
<?php endif; ?>

<?php
    $comments_args = array(
        'label_submit' => 'Enviar Comentario',
        'title_reply' => 'Escribe tu comentario',
        'comment_notes_after' => ''
    );
    comment_form($comments_args);
?>